# pexpect-executor

## Test on Linux

The following tools are required for Linux hosts tests (example: Linux Mint, Ubuntu, CentOS, ...):

- **PyPI :** <https://pip.pypa.io/en/stable/installing/>
- **gcil :** `pipx install --force gcil`

---

## Test on macOS

The following tools are required for macOS hosts tests:

- **Python :** Type `python3` in the Terminal app and install automatically
- **pexpect-executor :** `pip3 install -U pexpect-executor`
- **gcil :** `pip3 install -U gcil`

---

## Test on Windows

The following tools are required for Windows 10 hosts tests:

- **Git for Windows :** <https://gitforwindows.org> (Git, 64-bit.exe)
- **Python :** <https://www.python.org/downloads/windows/> (Python 3, x86-64 executable, all users, add to PATH, path length limit disabled)
- **pexpect-executor :** `pip3 install -U pexpect-executor`
- **pexpect :** `pip3 install -U pexpect`
- **gcil :** `pip3 install -U gcil`

---

## Test on Android

The following tools are required for Android hosts tests (example: 10, ...):

- **Termux :** Install the `Termux` application (<https://termux.com>)
- **OpenSSL :** Type `pkg install openssl` in the Termux app
- **Python :** Type `pkg install python` in the Termux app
- **pip :** Type `python3 -m pip install -U pip` in the Termux app
- **pexpect-executor :** `pip3 install -U pexpect-executor`
