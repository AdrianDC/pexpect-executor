#!/usr/bin/env python3

# Standard libraries
from time import sleep

# Header
print('=' * 8)
print(' header')
print('-' * 8)

# Delay
sleep(3)

# Footer
print('=' * 8)
print(' footer')
print('-' * 8)
